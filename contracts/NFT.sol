// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract NFT is ERC721, Ownable {
    error TokenDoesNotExists();

    uint16 public constant MAX_SUPPLY = 8000;

    string public baseTokenURI;

    /**
     * @notice Emitted when user minted nft.
     * @param _value The token URI.
     * @param _id The token id of collection nft.
     */
    event PermanentURI(string _value, uint256 indexed _id);

    /**
     * @notice Emitted when user minted nft.
     * @param _tokenId The token id of collection nft.
     * @param _buyer The user address.
     */
    event Minted(uint16 indexed _tokenId, address indexed _buyer);

    constructor(
        string memory name_,
        string memory symbol_,
        string memory baseTokenURI_
    ) ERC721(name_, symbol_) {
        baseTokenURI = baseTokenURI_;
    }

    /**
     * @notice Mints NFT by the token id.
     * @param _tokenId The token id of collection nft.
     */
    function mint(uint16 _tokenId) external {
        if (_tokenId < 1 || _tokenId > MAX_SUPPLY) {
            revert TokenDoesNotExists();
        }
        _safeMint(msg.sender, _tokenId);

        emit Minted(_tokenId, msg.sender);
        emit PermanentURI(tokenURI(_tokenId), _tokenId);
    }

    /**
     * @notice Checks token id on existence.
     * @param _tokenId The token id of collection nft.
     * @return Status if token id is exist or not.
     */
    function exists(uint256 _tokenId) external view returns (bool) {
        return _exists(_tokenId);
    }

    /**
     * @notice Returns the token uri by id.
     * @param _tokenId The token id of collection.
     * @return tokenURI.
     */
    function tokenURI(uint256 _tokenId)
        public
        view
        override
        returns (string memory)
    {
        if (!_exists(_tokenId)) {
            revert TokenDoesNotExists();
        }

        return
            string(
                abi.encodePacked(
                    baseTokenURI,
                    Strings.toString(_tokenId),
                    ".json"
                )
            );
    }
}
