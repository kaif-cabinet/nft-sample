# PROJECT DEPLOYMENT FLOW

1. Clone the project from GitLab
2. Install dependencies
3. Customize configurations
4. Deploy

# 1. Clone the project from GitLab

Enter the following command in the terminal:

```shell
git clone https://gitlab.com/kaif-cabinet/nft-sample.git
```

# 2. Install dependencies

Before launch next command open the terminal into the the main folder of project
Then, enter:

```shell
npm install
```

# 3. Customize configurations

In this project:

1. Rename the .env.example file to a file named .env
2. In the .env file change:

a) Set up API key
- if you deploy in goerli testnet you should set up your Etherscan API key

To get the Etherscan API key, go to
<a href="https://etherscan.io//myapikey">https://etherscan.io/myapikey </a>

b) Set up RPC URL
- if you deploy in goerli testnet you should set up GOERLI_TESTNET_RPC_URL for that you can use Infura or Alchemy

# 4. Deploy

# DEPLOY ON GOERLI TESTNET

```shell
npx hardhat run scripts/deploy-nft.ts --network goerli
```

# VERIFICATION

Verification is automated

