import hre, { ethers } from "hardhat";
import { NFT__factory } from "../typechain-types/factories/contracts/NFT__factory";

async function main() {
  const name = "Test Collection";
  const symbol = "TC";
  const baseTokenURI = "ipfs://bafkreib7rk44lfgqzt6jfvma4khx6sgag6edmp4d2avt67flk5wueqfjc4/";

  const delay = (ms: any) => new Promise((res) => setTimeout(res, ms));

  const Nft = (await ethers.getContractFactory('NFT')) as NFT__factory;
  const nft = await Nft.deploy(name, symbol, baseTokenURI);
  await nft.deployed();

  console.log("Nft deployed to:", nft.address);

  await delay(35000);

  await hre.run("verify:verify", {
    address: nft.address,
    constructorArguments: [],
  });
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
