import { expect } from "chai";
import { ethers } from "hardhat";
import { parseUnits } from "@ethersproject/units";
import { NFT__factory } from "../typechain-types/factories/contracts/NFT__factory";
import { NFT } from "../typechain-types/contracts/NFT";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

describe('NFT contract', () => {
  let nft: NFT;
  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addr3: SignerWithAddress;
  let addrs: SignerWithAddress[];

  const name = "Test Collection";
  const symbol = "TC";
  const baseTokenURI = "ipfs://bafkreib7rk44lfgqzt6jfvma4khx6sgag6edmp4d2avt67flk5wueqfjc4/";

  beforeEach(async () => {
    [owner, addr1, addr2, addr3, ...addrs] = await ethers.getSigners();

    const Nft = (await ethers.getContractFactory('NFT')) as NFT__factory;
    nft = await Nft.deploy(name, symbol, baseTokenURI);
    await nft.deployed();
  });

  describe('initial values', async () => {
    it('should set token name', async () => {
      expect(name).to.be.equal(await nft.name());
    });

    it('should set token symbol', async () => {
      expect(symbol).to.be.equal(await nft.symbol());
    });

    it('should set base token URI', async () => {
      expect(symbol).to.be.equal(await nft.symbol());
    });
  });

  describe('mints NFT by the token id', async () => {
    const tokenId = 1;

    it('mints NFT successfully', async () => {
      const addr1BalanceBefore = await ethers.provider.getBalance(addr1.address);

      const tx = await nft.connect(addr1).mint(tokenId);

      const minedTx = await tx.wait();
      const fee = minedTx.gasUsed.mul(minedTx.effectiveGasPrice);
      const uri = await nft.tokenURI(tokenId);

      const addr1BalanceAfter = await ethers.provider.getBalance(addr1.address);

      expect(addr1BalanceAfter).to.equal(addr1BalanceBefore.sub(fee));
      expect(tx).to.emit(nft, "Minted").withArgs(tokenId, addr1.address);
      expect(tx).to.emit(nft, "PermanentURI").withArgs(uri, tokenId);
    });

    it('rejects minting NFT while token id less than 1', async () => {
      await expect(nft.connect(addr1).mint(0)).to.be.revertedWith('TokenDoesNotExists()')
    });

    it('rejects minting NFT while token id more than 8000', async () => {
      await expect(nft.connect(addr1).mint(10006)).to.be.revertedWith('TokenDoesNotExists()')
    });
  });

  describe('gets token URI', async () => {
    it('gets token URI successfully', async () => {
      const tokenId = 1;

      await nft.connect(addr1).mint(tokenId);

      expect(baseTokenURI + tokenId + ".json").to.equal(await nft.tokenURI(tokenId));
    });

    it('rejects nonexistent token', async () => {
      await expect(nft.tokenURI(parseUnits("1000", 18))).to.be.revertedWith('TokenDoesNotExists()');
    });
  });

  describe('checks token id on existence', async () => {
    const tokenId = 1;

    it('checks token id on existence if it is true', async () => {
      await nft.connect(addr1).mint(tokenId);
      expect(await nft.exists(tokenId)).to.be.equal(true);
    });

    it('checks token id on existence if it is false', async () => {
      expect(await nft.exists(tokenId)).to.be.equal(false);
    });
  });
});
